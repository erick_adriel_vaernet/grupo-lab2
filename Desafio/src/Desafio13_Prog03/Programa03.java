package Desafio13_Prog03;

import java.util.Scanner;

public class Programa03 {

	public static void main(String[] args) {
		/*Crear una matriz que contenga datos de personas, siendo cada columna una persona diferente y cada fila los datos de las mismas. 
			Los datos para rellenar la matriz deben ser proporcionados por el usuario. 
			Una vez que se llene todo el arreglo de arreglos se debe mostrar la información en la consola de una forma clara.
			BONUS: ordenar alfabéticamente el arreglo.
		*/
		
		Scanner sc =new Scanner(System.in);
		
		//Se crea la matriz 3x3 
		String[][] matriz= new String[3][3];
		
		//Se pide al usuario los datos de las 3 personas.
		System.out.println("Debe ingresar el nombre, DNI y edad de 3 personas:");

		for (int i=0; i<3; i++) {


			/*Se agregaron tabulaciones al nombre y a la edad para que quede mejor a la vista (Se tuvo en cuenta que
			el DNI va actualmente entre el rango de 7 a 8 cifras y la Edad de entre 1 y 3 cifras)*/
			
			System.out.println("-------------------------------");
			System.out.printf("Nombre Persona %d:",i+1);
			matriz[0][i]= sc.next()+"	 ";

			System.out.printf("DNI Persona %d:",i+1);
			matriz[1][i]= sc.next();

			
			System.out.printf("Edad Persona %d:",i+1);
			matriz[2][i]= sc.next()+"	 ";
				
		}
		
		sc.close();
		
		//Se muestra la matriz con el formato fila1=nombre fila2=dni fila3=edad
		
		System.out.println("-------------------------------");
		System.out.println("");
		System.out.println("Matriz con formato:");
		for(String fila[]:matriz) {
			
			for(String elemento:fila) {

				System.out.print(elemento+" ");
			}
			System.out.println("");
		}
		
		
		
	}

}

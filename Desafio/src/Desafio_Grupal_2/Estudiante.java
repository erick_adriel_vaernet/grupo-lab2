package Desafio_Grupal_2;

public class Estudiante extends Persona{
	
	//------------------------Constructor-------------------------------
	
	public Estudiante(String nombre, String apellido, int legajo) {
		setNombre(nombre);
		setApellido(apellido);
		setLegajo(legajo);
	}
	

	//------------M�todo de la clase abstracta Persona------------------
	
	@Override
	public void modificarDatos () {
	}

	//--------------------------toStrings()----------------------------
	
	@Override
	public String toString() {
		return "Estudiante [Nombre()=" + getNombre() + ", Apellido()=" + getApellido() + ", Legajo()="+ getLegajo() + "\n ]";
	}
    	

}

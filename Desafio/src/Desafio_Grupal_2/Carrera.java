package Desafio_Grupal_2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Carrera implements Informacion{
	
	//-------------------------------Atributos---------------------------------
	
	private String nombre;
	private Set<Materia> coleccionMaterias= new HashSet<Materia>();
	
	//-----------------------------Constructor-------------------------------------
	
	public Carrera(String nombre) {
		this.nombre = nombre;
	}

	/*
	public Carrera(String nombre, String nombreMateria, Profesor titular) {
		this.nombre = nombre;
		Materia nuevaMateria= new Materia(nombreMateria,titular);
		coleccionMaterias.add(nuevaMateria);
	}
	*/
	
	//-----------------------------Geters y Seters---------------------------------
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Set<Materia> getColeccionMaterias() {
		return coleccionMaterias;
	}

	public void setColeccionMaterias(Set<Materia> coleccionMaterias) {
		this.coleccionMaterias = coleccionMaterias;
	}

	//------Implementacion de metodos correspondientes a la interfaz Informaci�n----------

	@Override
	public int verCantidad() {
		return this.coleccionMaterias.size();
	}

	@Override
	public String listarContenidos() {
		return this.coleccionMaterias.toString();
	}
	
	//--------------M�todo para agregar una materia a la carrera--------------------
	
	public void agregarMateria(String nombre, Profesor titular) {
		Materia nuevaMateria= new Materia(nombre, titular);
		coleccionMaterias.add(nuevaMateria);
		System.out.println("Se añadio la materia "+nombre+" a la carrera "+ this.nombre);
	}

	//--------------M�todo para eliminar una materia a la carrera--------------------
	
	public void eliminarMateria(String nombreMateria) {
		Iterator<Materia> iterador= this.coleccionMaterias.iterator();
		while(iterador.hasNext()) {
			Materia materia = iterador.next();
			if(materia.getNombre().equals(nombreMateria))
				iterador.remove();
			System.out.println("Se elimino la materia: "+nombreMateria);
		}
	}
	
	//--------------M�todo para encontrar una materia de la carrera--------------------
	public Materia encontrarMateria (String nombre) {
		for(Materia materia: this.coleccionMaterias) {
			if(materia.getNombre().equals(nombre))
				return materia;
		}
		return null;
	}

	//---------------Método para buscar una materia en la carrera------------------------------
	
		public Materia buscarMateria (String nombreMateria) {
			for(Materia materia : this.coleccionMaterias) {
				if(materia.getNombre().equals(nombreMateria));
				return materia;
			}
			System.out.println("No se encontro la materia correspondiente a la carrera "+this.nombre);
			return null;
		}
	
	
	//--------------------------------toStrings()-----------------------------------------
	@Override
	public String toString() {
		return "Carrera [nombre=" + nombre + ", coleccionMaterias=" + coleccionMaterias + "]";
	}
	
}

package Desafio_Grupal_2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Materia implements Informacion {
	
	//-------------------------------Atributos---------------------------------
	
	String nombre;
	Profesor titular;
	Set<Estudiante>coleccionEstudiantes= new HashSet<Estudiante>();
	
	//-----------------------------Constructor-------------------------------------
	
	public Materia(String nombre, Profesor titular) {
		super();
		this.nombre = nombre;
		this.titular = titular;
	}

	//-----------------------------Geters y Seters---------------------------------
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Profesor getTitular() {
		return titular;
	}

	public void setTitular(Profesor titular) {
		this.titular = titular;
	}

	//------Implementacion de metodos correspondientes a la interfaz Informaci�n----------
	
	@Override
	public int verCantidad() {
		return coleccionEstudiantes.size();
	}

	@Override
	public String listarContenidos() {
		return this.coleccionEstudiantes.toString();
	}
	
	//--------------M�todo para agregar un estudiante a la materia--------------------
	
	public void agregarEstudiante(String nombre, String apellido, int legajo) {
		Estudiante nuevoEstudiante= new Estudiante(nombre,apellido,legajo);
		coleccionEstudiantes.add(nuevoEstudiante);
		System.out.println("Se añadio "+nombre+", "+apellido+" a la materia"+ this.nombre);

	}
	
	//--------------M�todo para eliminar un estudiante de la materia--------------------
	
	public void eliminarEstudiante(int legajo) {
		Iterator<Estudiante> iterador= this.coleccionEstudiantes.iterator();
		while(iterador.hasNext()) {
			Estudiante estudiante = iterador.next();
			if(estudiante.getLegajo()==legajo) {
				iterador.remove();
				System.out.println("Se elimino el estudiante "+estudiante.getNombre()+", "+estudiante.getApellido()+", legajo:"+estudiante.getLegajo());	
			}
			else {
				System.out.println("No se encontro el estudiante, nro de legajo erroneo");
			}
		}
	}
	
	//--------------M�todo para modificar el titular de la materia--------------------
	
	public void modificarTitular (Profesor nuevoProfesor) {
		this.titular= nuevoProfesor;		
	}

	//---------------------------------toStrings()---------------------------------------
	@Override
	public String toString() {
		return "Materia [nombre=" + nombre + ", titular=" + titular + ", coleccionEstudiantes=" + coleccionEstudiantes
				+ "]";
	}


	
}


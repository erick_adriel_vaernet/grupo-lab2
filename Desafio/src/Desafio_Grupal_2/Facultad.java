package Desafio_Grupal_2;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class Facultad implements Informacion{
	
	//-------------------------------Atributos---------------------------------
	
	private String nombre;
	private Set<Carrera> coleccionCarreras= new HashSet<Carrera>();
	
	//-----------------------------Constructor-------------------------------------
	
	public Facultad(String nombre) {
		this.nombre = nombre;
	}

	//-----------------------------Geters y Seters---------------------------------
	
	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Set<Carrera> getColeccionCarreras() {
		return coleccionCarreras;
	}


	public void setColeccionCarreras(Set<Carrera> coleccionCarreras) {
		this.coleccionCarreras = coleccionCarreras;
	}	
	
	//------Implementacion de metodos correspondientes a la interfaz Informaci�n----------
	
	@Override
	public int verCantidad() {
		return coleccionCarreras.size();
	}

	@Override
	public String listarContenidos() {
		return this.coleccionCarreras.toString();
	}
	

	//--------------M�todo para agregar una carrera a la Facultad--------------------
	
	public void agregarCarrera(String nombreCarrera) {
		coleccionCarreras.add(new Carrera(nombreCarrera));
		System.out.println("Se agrego " +nombreCarrera+" a la facultad "+this.nombre);
	}
	
	//--------------M�todo para eliminar una carrera de la Facultad--------------------
	
	public void eliminarCarrera(String nombre) {
		boolean bandera = false;
		Iterator<Carrera> iterador= this.coleccionCarreras.iterator();
		while(iterador.hasNext()) {
			Carrera carrera = iterador.next();
			if(carrera.getNombre().equals(nombre))
				iterador.remove();
				bandera=true;			
		}
		if (bandera) {
			System.out.println("Se elimino la carrera "+nombre+" de la facultad "+this.nombre);
		}
		else {
			System.out.println("No se pudo eliminar la carrera: "+nombre+" de la facultad "+this.nombre );
		}
	}
	
	
	
	//---------------Método para buscar una carrera en la facultad------------------------------
	
	public Carrera buscarCarrera (String nombreCarrera) {
		for(Carrera carrera : this.coleccionCarreras) {
			if(carrera.getNombre().equals(nombreCarrera));
			return carrera;
		}
		System.out.println("No se encontro la carrera correspondiente a la faultad "+this.nombre);
		return null;
	}
	
	//--------------M�todo para eliminar un estudiante de la Facultad--------------------
	
	public void eliminarEstudiante(String nombreCarrera, String nombreMateria, int nroLegajo) {
		for(Carrera carrera:coleccionCarreras) {
			if(carrera.getNombre().equals(nombreCarrera)) {
				System.out.println("Materia encontrada");
				carrera.encontrarMateria(nombreMateria).eliminarEstudiante(nroLegajo);
			}
		}
	}

	//---------------------------------toStrings()---------------------------------------
	@Override
	public String toString() {
		return "Facultad [nombre=" + nombre + ", coleccionCarreras=" + coleccionCarreras + "]";
	}
	
	
}

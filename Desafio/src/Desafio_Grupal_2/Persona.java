package Desafio_Grupal_2;

public abstract class Persona {
	private String nombre;
	private String apellido;
	private int legajo;
	
	//-------------M�todo abstracto modificarDatos--------------
	
	public abstract void modificarDatos();

	//---------------------Geters y Seters--------------------------
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	//---------------------toStrings()-----------------------
	
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + ", legajo=" + legajo + "]";
	}
	
	

}

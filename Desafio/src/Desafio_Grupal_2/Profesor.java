package Desafio_Grupal_2;

public class Profesor extends Persona{
	
	//---------------------Atributos---------------------------------
	private double basico;
	private int antiguedad;
	
	//---------------------Constructor-------------------------------------
	
	public Profesor(String nombre, String apellido, int legajo, double basico, int antiguedad) {
		setNombre(nombre);
		setApellido(apellido);
		setLegajo(legajo);
		this.basico = basico;
		this.antiguedad = antiguedad;
	}
	
	//------------M�todo de la clase abstracta Persona------------------
	
	@Override
	public void modificarDatos () {
	}
	

	//---------------------Geters y Seters---------------------------------
	
	public double getBasico() {
		return basico;
	}


	public void setBasico(double basico) {
		this.basico = basico;
	}


	public int getAntiguedad() {
		return antiguedad;
	}


	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	
	//---------------------toStrings()-----------------------
	
	@Override
	public String toString() {
		return "Profesor [Nombre()=" + getNombre() + ", Apellido()=" + getApellido() + ", Legajo()"+getLegajo()+ "basico=" + basico + ", antiguedad=" + antiguedad+ "\n ]";
	}

	//--------------Calculo del sueldo segun antiguedad--------------------
	
	public double calcularSueldo() {
		return this.basico + this.antiguedad*( (this.basico*10) /100 );
	}

		
}

package Desafio_Grupal_2;

import java.util.Scanner;


public class Main {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		
		System.out.println("Programa creado para cumplir con las consignas  dadas por la materia Laboratorio-2"
				+ " de la carrera Tecnico Superior en Programación corresponddiente a la Facultad Regional de "
				+ "Resistencia de la Uiversidad Tecnológica Nacional");
		
//----------------------------------------------Punto 1-----------------------------------------------------------
		
		System.out.println("1- Agregar Carreras a una Facultad.");
		System.out.println("");
		
		//Se crea una facultad con la cual se trabajara a lo largo del programa
		Facultad csExactasNat= new Facultad("Facultad de Ciencias Exactas y Naturales");
	
		boolean repetir=true;
		
		while(repetir){

			System.out.println("Indique el nombre de la Carreara que desea aregar a la "+csExactasNat.getNombre()+" :");
			String carrera = sc.next();
			
			try {				
				csExactasNat.agregarCarrera(carrera);	
			
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que la carrera que indico no se haya agregado");
				repetir=true;
				continue;
			}
			
			System.out.println("¿Desea seguir agregando carreras? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
			
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe ndicar continuar con 'si' caso contrario con 'no')");
				repetir=true;
			}
			
		}
		

		//----------------------------------------------Punto 2-----------------------------------------------------------
				
		System.out.println("");
		System.out.println("2- Eliminar Carreras de la facultad. ");
		System.out.println("");
		System.out.println("Lista de carreras correspondientes a "+csExactasNat.getNombre());
		System.out.println(csExactasNat.listarContenidos());
		
		repetir=true;
		
		while(repetir) {

			System.out.println("Ingrese la carrera a eliminar: ");
			String delCarrera = sc.next();
			
			try {				
				csExactasNat.eliminarCarrera(delCarrera);	
			
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que la carrera que indico no exista o haya ingresado mal el nombre");
				repetir=true;
				continue;
			}
			
			
			System.out.println("¿Desea seguir eliminando carreras? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
				else {
					System.out.println("No selecciono ni 'si' ni 'no', por defecto no se seguira eliminando carreras");
				}
			
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe ndicar continuar con 'si' caso contrario con 'no')por defecto no se seguira eliminando carreras");
				repetir=false;
			}	
		}
		
		
//------------------------------------------------Punto 4-----------------------------------------------------------
		
		//Se crean los profesores disonibles para las materias
		Profesor uferer = new Profesor("Facundo","Uferer",12345,100000,5);
		Profesor matoff = new Profesor("Facundo","Matoff",54321,100000,5);
		
		//Se procede con el punto 4:
		
		System.out.println("");
		System.out.println("4- Agregar materias a una Carrera.");
		System.out.println("");
		
		repetir=true;
		while(repetir){
			System.out.println("Lista de Carreras:");
			csExactasNat.listarContenidos();
			System.out.println("Ingrese el nombre de la Carrera en la que desea agregar la materia:");
			String carreraSelec=sc.next();
			
			Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
			if(carreraElegida.equals(null)) {
				System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
				continue;
			}
			
			System.out.println("*Solo hay 2 profesores disponibles para dictar la materia*");
			System.out.println("1-"+uferer.getNombre()+" "+uferer.getApellido());
			System.out.println("2-"+matoff.getNombre()+" "+matoff.getApellido());
			System.out.println("Elija uno de ellos ingresando 1 o 2 segunn corresponda:");
			String eleccionProf=sc.next();
			

			System.out.println("Indique el nombre de la materia que desea aregar a la "+csExactasNat.getNombre()+" :");
			String materia = sc.next();
			
			try {
				if(eleccionProf.equals("1")) {
					carreraElegida.agregarMateria(materia,uferer);
				}
				else if(eleccionProf.equals("2")) {
					carreraElegida.agregarMateria(materia,matoff);
				}else {
					System.out.println("Ingreso un numero que no corresponde");
					continue;
				}	
			
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que la materia que indico no se haya agregado");
				continue;
			}
			
			System.out.println("¿Desea seguir agregando materias? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
				else {
					System.out.println("No ingreso si/no, por defecto no se seguira agregando");
					repetir=false;
					continue;
				}
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira agregando materias");
				repetir=false;
			}
			
		}

//--------------------------------------------------------Punto 5---------------------------------------------------------------
		
		System.out.println("");
		System.out.println("5-Eliminar materias a una Carrera.");
		System.out.println("");
		
		repetir=true;
		while(repetir){
			System.out.println("Seleccione la carrera en la que desea eliminar una materia:");
			System.out.println("Lista de Carreras:");
			csExactasNat.listarContenidos();
			System.out.println("Ingrese el nombre de la Carrera para eliminar una materia:");
			String carreraSelec=sc.next();
			
			Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
			
			if(carreraElegida.equals(null)) {
				System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
				continue;
			}
			
			System.out.println("Selecciono la carrera "+carreraElegida.getNombre()+". Ahora seleccione la materia a eliminar");
			
			System.out.println("Lista de Materias de "+carreraElegida.getNombre()+":");
			carreraElegida.listarContenidos();
			System.out.println("");
			
			System.out.println("Ingrese el nombre de la materia que desea eliminar de la carrera:");
			String materiaSelec=sc.next();
			
			try {
				carreraElegida.eliminarMateria(materiaSelec);
			}catch (Exception e){
				System.out.println("Error: La materia que intenta eliminar no existe.");
				continue;
			}
			
			System.out.println("¿Desea seguir eliminando materias? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
				else {
					System.out.println("No ingreso si/no, por defecto no se seguira eliminando materias");
					repetir=false;
					continue;
				}
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira eliminando materias");
				repetir=false;
			}
			
		}	
		
//--------------------------------------------------------Punto 6---------------------------------------------------------------

		System.out.println("");
		System.out.println("6-Encontrar materias de una carrera en particular indicando el nombre de la materia. Si la materia existe nos deberá preguntar si deseamos eliminar.");
		System.out.println("");

		//Se resolvio de manera muy similar a la anterior con la diferencia que en este punto se busca con el metodo buscar materia si existe y luego se pregunta para eliminarla		

		repetir=true;
		while(repetir){
			System.out.println("Seleccione la carrera en la que desea eliminar una materia:");
			System.out.println("Lista de Carreras:");
			csExactasNat.listarContenidos();
			System.out.println("Ingrese el nombre de la Carrera para eliminar una materia:");
			String carreraSelec=sc.next();
			
			Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
			
			if(carreraElegida.equals(null)) {
				System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
				continue;
			}
		
			System.out.println("Selecciono la carrera "+carreraElegida.getNombre());
			
			System.out.println("Ingrese el nombre de la materia que desea eliminar de la carrera:");
			String materiaSelec=sc.next();
			

			try {
				Materia materiaElegida = carreraElegida.buscarMateria(materiaSelec);
				if(!materiaElegida.equals(null))
				{	
					System.out.println("Se encontro la Materia,¿Desea eliminarla?");
					String elim=sc.next();
					
					if(elim.equals("si")) 
					{
						carreraElegida.eliminarMateria(materiaElegida.getNombre());
						System.out.println("Se elimino la materi");
					}else {
						System.out.println("Eligio no eliminar la materia");
					}
				}
				else {
					System.out.println("Materia no encontrada");
				}
			}catch (Exception e){
				System.out.println("Error: ingreso erroneamente algun valor");
				continue;
			}
			
			System.out.println("¿Desea seguir eliminando materias? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
				else {
					System.out.println("No ingreso si/no, por defecto no se seguira eliminando materias");
					repetir=false;
					continue;
				}
			}catch (Exception e) {
				System.out.println("¡Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira eliminando materias");
				repetir=false;
			}
			
		}

//--------------------------------------------------------Punto 7---------------------------------------------------------------
				
				System.out.println("");
				System.out.println("7-Agregar Estudiantes a una Materia.");
				System.out.println("");
				
				repetir=true;
				while(repetir){
					System.out.println("Seleccione la carrera en la que desea agregar un estudiante:");
					System.out.println("Lista de Carreras:");
					csExactasNat.listarContenidos();
					System.out.println("Ingrese el nombre de la Carrera para luego seleccionar la materia en la que se agregara al estudiante:");
					String carreraSelec=sc.next();
					
					Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
					
					if(carreraElegida.equals(null)) {
						System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
						continue;
					}
					
					System.out.println("Selecciono la carrera "+carreraElegida.getNombre()+". Ahora seleccione la materia donde se agregara al estudiante");
					
					System.out.println("Lista de Materias de "+carreraElegida.getNombre()+":");
					carreraElegida.listarContenidos();
					System.out.println("");
					
					System.out.println("Ingrese el nombre de la materia en la lque desea agregar un estudiante:");
					String materiaSelec=sc.next();
					
					Materia materiaElegida  = carreraElegida.buscarMateria(materiaSelec);
					
					if(materiaElegida.equals(null)) {
						System.out.println("La materia ingresada no existe. Reintente ingresando otra Materia");
						continue;
					}
					
					
					try {
						System.out.println("Ingrese el Nombre del alumno:");
						String nombreAlumno=sc.next();
						System.out.println("Ingrese el Apellido del alumno:");
						String apellidoAlumno=sc.next();
						System.out.println("Ingrese la matricula o legajo del alumno:");
						int legajoAlumno=sc.nextInt();
						
						Estudiante nuevoAlumno= new Estudiante(nombreAlumno,apellidoAlumno,legajoAlumno);
						materiaElegida.coleccionEstudiantes.add(nuevoAlumno);
					}catch (Exception e){
						System.out.println("Error: No se pudo agregar el alumno. Reintente ingresando correctamente los datos");
						continue;
					}
					
					System.out.println("�Desea seguir agregando estudiantes? (si/no) :");
					String repe = sc.next();
					
					try {				
						if (repe.equals("si"))
							repetir=true;
						else if (repe.equals(("no")))
							repetir=false;
						else {
							System.out.println("No ingreso si/no, por defecto no se seguira agregando estudiantes");
							repetir=false;
							continue;
						}
					}catch (Exception e) {
						System.out.println("�Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira agregando estudiantes");
						repetir=false;
					}
					
				}

//--------------------------------------------------------Punto 8---------------------------------------------------------------
				
				System.out.println("");
				System.out.println("8-Eliminar Estudiantes de una Materia.");
				System.out.println("");
				
				repetir=true;
				while(repetir){
					System.out.println("Seleccione la carrera en la que desea eliminar un estudiante:");
					System.out.println("Lista de Carreras:");
					csExactasNat.listarContenidos();
					System.out.println("Ingrese el nombre de la Carrera para luego seleccionar la materia en la que se eliminara al estudiante:");
					String carreraSelec=sc.next();
					
					Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
					
					if(carreraElegida.equals(null)) {
						System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
						continue;
					}
					
					System.out.println("Selecciono la carrera "+carreraElegida.getNombre()+". Ahora seleccione la materia donde se eliminara al estudiante");
					
					System.out.println("Lista de Materias de "+carreraElegida.getNombre()+":");
					carreraElegida.listarContenidos();
					System.out.println("");
					
					System.out.println("Ingrese el nombre de la materia en la lque desea eliminar un estudiante:");
					String materiaSelec=sc.next();
					
					Materia materiaElegida  = carreraElegida.buscarMateria(materiaSelec);
					
					if(materiaElegida.equals(null)) {
						System.out.println("La materia ingresada no existe. Reintente ingresando otra Materia");
						continue;
					}
					
					
					try {
						
						System.out.println("Ingrese la matricula o legajo del alumno a eliminar:");
						int legajoAlumno=sc.nextInt();
						materiaElegida.eliminarEstudiante(legajoAlumno);
						
					}catch (Exception e){
						System.out.println("Error: No se pudo eliminar el alumno. Reintente ingresando correctamente los datos");
						continue;
					}
					
					System.out.println("�Desea seguir eliminando estudiantes? (si/no) :");
					String repe = sc.next();
					
					try {				
						if (repe.equals("si"))
							repetir=true;
						else if (repe.equals(("no")))
							repetir=false;
						else {
							System.out.println("No ingreso si/no, por defecto no se seguira eliminando estudiantes");
							repetir=false;
							continue;
						}
					}catch (Exception e) {
						System.out.println("�Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira eliminanddo estudiantes");
						repetir=false;
					}
					
				}	
				
			
//--------------------------------------------------------Punto 9---------------------------------------------------------------
		
		System.out.println("");
		System.out.println("9-Modificar el Profesor de la materia. ");
		System.out.println("");
		
		repetir=true;
		while(repetir){
			System.out.println("Seleccione la carrera en la que desea modificar un profesor:");
			System.out.println("Lista de Carreras:");
			csExactasNat.listarContenidos();
			System.out.println("Ingrese el nombre de la Carrera para luego seleccionar la materia en la que se modificara el profesor:");
			String carreraSelec=sc.next();
			
			Carrera carreraElegida  = csExactasNat.buscarCarrera(carreraSelec);
			
			if(carreraElegida.equals(null)) {
				System.out.println("La carrera ingresada no existe. Reintente ingresando otra carrera");
				continue;
			}
			
			System.out.println("Selecciono la carrera "+carreraElegida.getNombre()+". Ahora seleccione la materia donde se modificara el profesor");
			
			System.out.println("Lista de Materias de "+carreraElegida.getNombre()+":");
			carreraElegida.listarContenidos();
			System.out.println("");
			
			System.out.println("Ingrese el nombre de la materia en la que desea modificar el profesor");
			String materiaSelec=sc.next();
			
			Materia materiaElegida  = carreraElegida.buscarMateria(materiaSelec);
			
			if(materiaElegida.equals(null)) {
				System.out.println("La materia ingresada no existe. Reintente ingresando otra Materia");
				continue;
			}
			
			
			try {
				
				System.out.println("Ingrese los datos del nuevo profesor:");
				System.out.println("Ingrese el Nombre del profesor:");
				String nombreProfesor=sc.next();
				System.out.println("Ingrese el Apellido del profesor:");
				String apellidoProfesor=sc.next();
				System.out.println("Ingrese la matricula o legajo del profesor:");
				int legajoProfesor=sc.nextInt();
				System.out.println("Ingrese el sueldo base del profesor:");
				int sueldoBaseProfesor=sc.nextInt();
				
				Profesor nuevoProfesor = new Profesor(nombreProfesor,apellidoProfesor,legajoProfesor,sueldoBaseProfesor,0);
				
				materiaElegida.modificarTitular(nuevoProfesor);
			}catch (Exception e){
				System.out.println("Error: No se pudo agregar el nuevo profesor. Reintente ingresando correctamente los datos");
				continue;
			}
			
			System.out.println("�Desea seguir agregando profesores? (si/no) :");
			String repe = sc.next();
			
			try {				
				if (repe.equals("si"))
					repetir=true;
				else if (repe.equals(("no")))
					repetir=false;
				else {
					System.out.println("No ingreso si/no, por defecto no se seguira agregando profesores");
					repetir=false;
					continue;
				}
			}catch (Exception e) {
				System.out.println("�Algo salio mal! es probable que haya ingresado mal la respuesta (Se debe indicar continuar con 'si' caso contrario con 'no'), por defecto no se seguira agregando profesores");
				repetir=false;
			}
			
		}
		sc.close();

		System.out.println("*---Fin del programa---*");
	}

}

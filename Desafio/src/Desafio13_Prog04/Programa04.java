package Desafio13_Prog04;

public class Programa04 {

	public static void main(String[] args) {
	
		/*Realice un programa que genere una matriz de 3x3 con números al azar y la muestra utilizando la consola. 
		Una vez que muestre la matriz desordenada, debe mostrar también la matriz ordenada de menor a mayor*/
		
		
		//Se crea una matriz 3x3.
		int[][] matriz = new int[3][3];
		
		int[] arreglo_aux = new int[9]; //Array auxiliar que srvira en el ordenamiento de la matriz.
		int aux;		//variable auxiliar que servira a la hora de ordenar la matriz.
		int q=0;
		
		//Se procede a llenarla con numeros al azar entre 1 y 100.
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				
				matriz[i][j] = (int) (Math.random()*(100+1));
			}
		}
		
		
		//Se muestra la matriz desordenada.
		
		System.out.println("Matriz con numeros random:");
		
		for(int fila[]:matriz) {
			
			for(int elem:fila) {
				
				System.out.print(elem+" ");
			}
			System.out.println();
		}
		
	
		//Se ordena la matriz de menor a mayor, para ello:
		
		//1-Copiamos la matriz en un arreglo.
		
		
			
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				
				arreglo_aux[q] = matriz[i][j];
				
				q++; //"q" aumenta de 0 a 8 al finalizar ambos bucles for (cargando los 9 elementos de la matriz)
			}
		}
		
		//2- Ordenamos el arreglo (basado en ordenamiento burbuja).
		for(int i=1; i<9; i++)
		{
			for(int j=0; j<9-i; j++)
			{
				if(arreglo_aux[j]>arreglo_aux[j+1])
				{
					aux    = arreglo_aux[j+1];
					arreglo_aux[j+1] = arreglo_aux[j];
					arreglo_aux[j]   = aux;
				}
			}
		}	
		
		
	
		//3- Copiamos los elementos ya ordenados desde el arreglo a la matriz.
	
		q=0;
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				
				matriz[i][j]=arreglo_aux[q];
				
				q++; 
			}
		}
		
		
		System.out.println("");
		
		//Por ultimo Se muestra la matriz ordenada.
		
		System.out.println("Matriz Ordenadade menor a mayor:");
		
		for(int fila[]:matriz) {
			
			for(int elem:fila) {
				
				System.out.print(elem+" ");
			}
			System.out.println();
		}

	
	
	}

}

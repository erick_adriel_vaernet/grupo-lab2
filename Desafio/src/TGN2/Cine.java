package TGN2;

import java.util.ArrayList;
import java.util.Scanner;



public class Cine {

	public static void main(String[] args) {
	
		System.out.println("*Bienvenido al Cine*");
	
		System.out.println("Se realizara la carga de Espectadores:");
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("¿Cuantos espectadores desea ingresar?");
		int cantidad= sc.nextInt();
		
		ArrayList <Espectadores> nuevosEspectadores= new ArrayList<Espectadores>();
		
		for(int i=0; i<cantidad;i++){
		
			try {
				
				System.out.println("Ingrese el nombre del espectador:");
				String nombreEspectador= sc.next();

				System.out.println("Ingrese la edad del espectador:");
				int edadEspectador= sc.nextInt();

				System.out.println("Ingrese la fila del espectador:");
				String filaEspectador= sc.next();

				System.out.println("Ingrese la silla del espectador:");
				int sillaEspectador= sc.nextInt();
				
				nuevosEspectadores.add( new Espectadores(nombreEspectador,edadEspectador,filaEspectador, sillaEspectador));
				
			}catch(Exception e) {
				
				System.out.println("ERROR EN EL INGRESO DE DATOS.");
			}
			
		}
		
		Salas sala01 = new Salas( 3, "sala01");
		sala01.setPelicula("Joker");
		
		sala01.setEspectadores(nuevosEspectadores);
		
		//Se imprimenr espectadores:
		System.out.println("Espectadores Cargados en "+sala01.getNombre()+":");
		
		for(Espectadores espectador: sala01.getEspectadores()) 
		{
			System.out.println(espectador);		
		}
		
		//Se asigna el acomodador
		Acomodadores Uferer = new Acomodadores ("Uferer", 30);
		
		Uferer.setSala(sala01);
		Uferer.setSueldo(50000);
		System.out.println(Uferer.toString());
		Empleados Leo = new Empleados ("Leo", 25) ;
		System.out.println(Leo.toString());
        	
  
		
		
		//-------------------------------------------------------------------------------
		sc.close();
	}

}

package TGN2;

public class Acomodadores extends Empleados implements ParaAcomodadores{

	//----------------------------Atributos--------------------
	private Salas sala;
	
	//----------------------------Constructor-------------------------
	public Acomodadores(String nombre, int edad) {
		super(nombre, edad);
		
	}

	//---------------------------Implementacion de m�todos de la interfaz ParaAcomodadores-------------------
	@Override
	public Salas getSala() {
		return this.sala;
	}

	@Override
	public void setSala(Salas sala) {
		this.sala=sala;
	}
	
	//-------------------Metodos abstractos de Persona-------------------------
		@Override
		public String getTipo() {
			return "Acomodadores";
		}

		@Override
		public String toString() {
			return "Acomodadores [Nombre=" + this.getNombre() + ", Edad=" + this.getEdad()+ ", sueldo=" + this.getSueldo()+ "]";
		}
	
	
}

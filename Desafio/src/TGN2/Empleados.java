package TGN2;

public class Empleados extends Persona {
	
	//------------------------Atributos-------------------------------
	
	private double sueldo;
	
	//-----------------------Cosntructor-----------------------------
	
	public Empleados(String nombre, int edad) {
		
		super.setNombre(nombre);
		super.setEdad(edad);
		this.sueldo = 20000;
		
		
	}

	//-------------------Getters and Setters------------------------

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
		
	}

	//-------------------Metodos abstractos de Persona-------------------------
	@Override
	public String getTipo() {
		return "Empleados";
	}

	@Override
	public String toString() {
		return "Empleados [Nombre=" + this.getNombre() + ", Edad=" + this.getEdad()+ ", sueldo=" + this.getSueldo()+ "]";
	}
	

}

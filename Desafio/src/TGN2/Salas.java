package TGN2;

import java.util.ArrayList;

public class Salas {
	
	//-----------------------Atributos---------------------------------
	int capacidad;
	String Pelicula;
	String nombre;
	ArrayList <Espectadores> Espectadores= new ArrayList<Espectadores>();
	
	

	public Salas(int capacidad, String nombre) {
		super();
		this.capacidad = capacidad;
		this.nombre = nombre;
	}
	//-----------------------Getters and Setters-----------------------
	
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	public String getPelicula() {
		return Pelicula;
	}
	public void setPelicula(String pelicula) {
		Pelicula = pelicula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Espectadores> getEspectadores() {
		return Espectadores;
	}
	public void setEspectadores(ArrayList<Espectadores> espectadores) {
		if(espectadores.size()<= this.capacidad ) {
			Espectadores = espectadores;
		}
		else {
			System.out.printf("\nSe supero la capacidad de la sala.\n"
					+ " Cantidad de espectadores ingresados:"+espectadores.size()+"\n"+
					"Capacidad de la sala:"+this.capacidad+"\n");
		}
	}

	
	//--------------------------toString()--------------------------

	@Override
	public String toString() {
		return "Salas [capacidad=" + capacidad + ", Pelicula=" + Pelicula + ", nombre=" + nombre + ", Espectadores="
				+ Espectadores + "]";
	}

	
}

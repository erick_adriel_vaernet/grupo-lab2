package TGN2;

public class Espectadores extends Persona{

//----------------Atributos--------------------------
	
	private String fila;
	private int silla;
	
//----------------Constructor--------------------------
	
	public Espectadores(String nombre, int edad, String fila, int silla) {
		super.setNombre(nombre);
		super.setEdad(edad);
		this.fila = fila;
		this.silla = silla;
	}

//----------------------Geters and Seters----------------------------------
	
	public String getFila() {
		return fila;
	}

	public void setFila(String fila) {
		this.fila = fila;
	}

	public int getSilla() {
		return silla;
	}

	public void setSilla(int silla) {
		this.silla = silla;
	}

//------------------Metodos de objetos de tipo Espectadores-------------------------
	
	public String getButaca() {
		return this.getFila()+ this.getSilla();
	};
	
//------------------Metodos abstractos de Persona-------------------------
	
	@Override
	public String getTipo() {
		return "Espectadores";
	}
	
	@Override
	public String toString() {
		return "Espectadores [Nombre=" + this.getNombre()+ ", Edad=" + this.getEdad()+ ", Fila=" + this.getFila()+ ", Silla="
				+ getSilla()+"]";
	}
	
	
	
}

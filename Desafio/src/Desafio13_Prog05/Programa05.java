package Desafio13_Prog05;

import java.util.Arrays;
import java.util.Scanner;

public class Programa05 {

	public static void main(String[] args) {
		//Realice un programa que genere una matriz de 3x3 con n�meros ingresados por el usuario por medio de la consola. 

		//Una vez terminada la carga de los elementos se debe mostrar primero la matriz cargada con los datos iniciales y luego la matriz con los datos ordenados de mayor a menor.

		Scanner sc=new Scanner(System.in);
		
		int [][] matriz= new int[3][3];
		
		int[] arreglo_aux = new int[9]; 
			
		int q=0;
 
		
		//Se le pide los datos al usuario
		
		System.out.println("Cargue la matriz:");
		
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				System.out.printf("\nEelemento [%d][%d]:",i,j);
				matriz[i][j] = sc.nextInt();
			}
		}
				
		sc.close();
			
		
		
		System.out.println("Matriz con numeros random:");
		
		for(int fila[]:matriz) {
			
			for(int elem:fila) {
				
				System.out.print(elem+" ");
			}
		System.out.println();
		}
		
		
		
		//Se ordena la matriz de mayor a menor 
		
			
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				
				arreglo_aux[q] = matriz[i][j];
				
				q++; 
			}
		}
		
		//2- Ordenamos el arreglo (basado en ordenamiento burbuja)
		Arrays.sort(arreglo_aux);
		
	
		//3- Copiamos los elementos ya ordenados desde el arreglo a la matriz
	
		q=0;
		for(int i=0;i<3; i++) {
			
			for(int j=0; j<3; j++) {
				
				matriz[i][j]=arreglo_aux[q];
				
				q++; 
			}
		}
		
		
		System.out.println("");
		
		
		
		System.out.println("Matriz Ordenadade menor a mayor:");
		
		for(int fila[]:matriz) {
			
			for(int elem:fila) {
				
				System.out.print(elem+" ");
			}
			System.out.println();
		}

			
	}

}
